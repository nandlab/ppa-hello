# Hello World

# Add PPA
```
curl 'https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xD80E587E7E12369C' | gpg --dearmor | sudo tee /usr/share/keyrings/elaunch-ppa-focal.gpg > /dev/null
echo 'deb [signed-by=/usr/share/keyrings/elaunch-ppa-focal.gpg] https://ppa.launchpadcontent.net/elaunch/ppa/ubuntu focal main' | sudo tee /etc/apt/sources.list.d/elaunch-ppa-focal.list > /dev/null
```

# Install
```
sudo apt update
sudo apt install ppa-hello
```

# Usage
```
ppa-hello [--help | --version]
```
